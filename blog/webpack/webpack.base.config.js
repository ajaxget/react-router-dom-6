const path = require('path');
const P = (_path) => path.resolve(__dirname, _path);
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackBar = require('webpackbar');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const publicPath = 'http://8.141.157.121/blog/';

const baseConfig = {
  // 定义多个入口,想要实现热加载的放在最后面
  entry: {
    commJS: P('../src/commJS/index.js'),
    index: P('../src/index.js'),
  },
  output: {
    filename: 'js/[name].js',
    path: P('../build'),
    // publicPath:所有资源(js,css,图片.....),公共URL前缀
    publicPath,
    clean: true,
    assetModuleFilename: 'assets/[hash][ext][query]',
  },
  optimization: {
    // 配置 optimization.moduleIds，让公共包 splitChunks 的 hash 不因为新的依赖而改变，减少非必要的 hash 变动
    moduleIds: 'deterministic',
  },
  module: {
    rules: [
      {
        test: /\.html$/i,
        use: 'html-loader',
      },
      {
        test: /\.(js|jsx)$/i,
        use: 'babel-loader',
        exclude: /node_modules/,
      },

      {
        test: /\.(png|jfif|jpg|svg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(ts|tsx)$/i,
        exclude: /node_modules/,
        use: ['ts-loader'],
      },
      {
        test: /\.scss$/i,
        use: [
          // 将 JS 字符串生成为 style 节点
          'style-loader',
          // 将 CSS 转化成 CommonJS 模块
          'css-loader',
          // 将 Sass 编译成 CSS
          'sass-loader',
        ],
      },
      {
        test: /\.(css|less)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                modifyVars: {},
                javascriptEnabled: true,
              },
            },
          },
        ],
      },
      {
        test: /\.md$/,
        loader: 'raw-loader',
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: P('../public/index.html'),
      scriptLoading: 'blocking',
      favicon: path.resolve(__dirname, '../public/favicon.ico'),
    }),
    new ESLintPlugin(),
    new WebpackBar({
      name: 'Daily Learning',
      color: 'green',
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name]_[chunkhash:8].css',
    }),
  ],
  resolve: {
    alias: {
      '@': P('../src'),
      '@axios': P('../src/config/http.ts'),
    },
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
  },
};

module.exports = {
  baseConfig,
  P,
};
