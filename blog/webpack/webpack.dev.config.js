const { merge } = require('webpack-merge');
const { baseConfig, P } = require('./webpack.base.config');
const webpack = require('webpack');

const port = 8082;

module.exports = merge(baseConfig, {
  mode: 'development',
  devtool: 'source-map',
  output: {
    publicPath: '/',
  },
  cache: {
    type: 'filesystem', // 使用文件缓存,提升构建速度,第一次构建会加大构建速度
  },

  devServer: {
    static: {
      directory: P('../build'),
      serveIndex: true,
    },
    compress: true,
    host: '127.0.0.1',
    hot: true,
    port,
    open: true,
    historyApiFallback: true,
    client: {
      overlay: false, // 当出现编译错误或警告时，在浏览器中显示全屏覆盖。
    },
    proxy: {
      '/Ax-blog': {
        target: 'http://8.141.157.121/Ax-Blog',
        pathRewrite: {},
        secure: false,
      },
      '/md': {
        target: 'http://8.141.157.121/skin',
        pathRewrite: {},
        secure: false,
      },
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      // 定义环境变量
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
  ],
});
