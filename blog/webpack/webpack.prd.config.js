const { merge } = require('webpack-merge');
const { baseConfig } = require('./webpack.base.config');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin'); // css 压缩优化

const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin'); // Webpack自带,无需安装(JS 压缩)

module.exports = merge(baseConfig, {
  mode: 'production',
  optimization: {
    // 可以将公共的依赖模块提取到已有的入口 chunk 中，或者提取到一个新生成的 chunk。
    splitChunks: {
      chunks: 'all',
      // 重复打包问题
      cacheGroups: {
        vendors: {
          // node_modules里的代码
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          // name: 'vendors', 一定不要定义固定的name
          priority: 10, // 优先级
          enforce: true,
        },
      },
    },
    minimizer: [
      // 在 webpack@5 中，你可以使用 `...` 语法来扩展现有的 minimizer（即 `terser-webpack-plugin`）
      new CssMinimizerPlugin(),
      new TerserPlugin({
        parallel: 4,
        terserOptions: {
          parse: {
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true,
          },
        },
      }),
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      // 定义环境变量
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  ],
});
