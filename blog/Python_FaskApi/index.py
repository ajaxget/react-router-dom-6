import uvicorn
from fastapi import FastAPI

from routers import file
from routers import notes
from routers import users

PREFIX = '/Ax-blog'

app = FastAPI(
    title="Ax_Blog",
    description="",
    version="1.0",
    terms_of_service="http://example.com/terms/",
    # 联系
    contact={
        "name": "Ax",
        "url": "https://gitee.com/ajaxget",
        "email": "13171754463@163.com",
    },
    # 许可证
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)

app.include_router(users.router, prefix=PREFIX)
app.include_router(file.router, prefix=PREFIX)
app.include_router(notes.router, prefix=PREFIX)

if __name__ == '__main__':
    uvicorn.run(app=app, host="127.0.0.1", port=8080)
