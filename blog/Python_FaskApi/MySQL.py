import MySQLdb


class MySql:
    def __init__(self):
        # 连接数据库;
        self.__connect()

    def __connect(self):
        self.db = MySQLdb.connect(host="8.141.157.121", port=3306, user="root", passwd="hdk021009", db="blog",
                                  charset="utf8")
        self.cursor = self.db.cursor()

    def execute(self, sql):
        # 执行sql语句
        self.cursor.execute(sql)
        # 同步到数据库
        self.db.commit()
        # 获取全部数据
        data = self.cursor.fetchall()
        return data

    def close(self):
        self.db.close()

    def __del__(self):
        self.close()
