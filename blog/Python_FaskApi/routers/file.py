import time
from fastapi import APIRouter, File, UploadFile

router = APIRouter(
    prefix="/file",
    tags=['传输']
)


@router.post('/file')
async def upload_file(file: UploadFile = File(default=None)):
    start = time.time()
    try:
        res = await file.read()
        with open(file.filename, "wb") as f:
            f.write(res)
        return {"message": "success", 'time': time.time() - start, 'filename': file.filename}
    except Exception as e:
        return {"message": str(e), 'time': time.time() - start, 'filename': file.filename}
