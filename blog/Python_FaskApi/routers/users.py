from fastapi import APIRouter
from pydantic import BaseModel

from MySQL import MySql
from tools import Auto_quote

db = MySql()

# 数据表
TABLE_NAME = 'users'

router = APIRouter(
    prefix="/users",
    tags=['users']
)


class Login(BaseModel):
    username: str
    password: str


# 登录
@router.post('/login')
def login(item: Login):
    """登录"""
    username = item.username
    password = item.password
    data = db.execute(
        'select username from %s where (username=\'%s\' and password=\'%s\')' % (TABLE_NAME, username, password))
    if len(data) > 0:
        return {
            "code": 0,
            "message": "登录成功"
        }
    else:
        return {
            "code": 1,
            "message": "用户名/密码错误!"
        }


class Signup(BaseModel):
    username: str
    password: str
    phone: str
    okPassword: str


# 注册
@router.post('/signup')
def signup(item: Signup):
    """注册"""
    if item.password != item.okPassword:
        return {
            'code': 1,
            'message': '请检查密码是否正确!'
        }
    for i in db.execute('show tables;'):
        table, = i
        if table == TABLE_NAME:
            break
    else:
        db.execute(
            'create table %s(id int AUTO_INCREMENT,username varchar(20) unique,password varchar('
            '20),phone varchar(11) unique,PRIMARY KEY (id))' % TABLE_NAME)

    max_id, = db.execute('select max(id) from %s;' % TABLE_NAME)[0]
    if max_id:
        if len(db.execute('select username from %s where username=%s' % (TABLE_NAME, Auto_quote(item.username)))) > 0:
            return {
                'code': 1,
                'message': '用户 %s 已经被创建' % item.username
            }
        if len(db.execute('select phone from %s where phone=%s' % (TABLE_NAME, Auto_quote(item.phone)))) > 0:
            return {
                'code': 1,
                'message': '手机号 %s 已经被使用' % item.phone
            }
        db.execute('insert into %s(id,username,password,phone) values (%d,%s,%s,%s)' % (
            TABLE_NAME, max_id + 1, Auto_quote(item.username), Auto_quote(item.password), Auto_quote(item.phone)))
    else:
        db.execute('insert into %s (id,username,password,phone) values (1,%s,%s,%s)' % (
            TABLE_NAME, Auto_quote(item.username), Auto_quote(item.password), Auto_quote(item.phone)))
    return {
        'code': 0,
        "message": '注册成功'
    }


# 重置密码
class Rpw_email(BaseModel):
    username: str
    oldPassword: str
    newPassword: str
    phone: str


@router.post('/rpw_email')
def Rpw_email(item: Rpw_email):
    """重置密码"""
    if item.oldPassword == item.newPassword:
        return {
            "code": 1,
            "message": '旧密码不可和新密码相同!'
        }
    if len(db.execute('select * from %s where username=%s' % (TABLE_NAME, Auto_quote(item.username)))) < 1:
        return {
            "code": 1,
            "message": "用户 %s 不存在!" % item.username,
        }
    elif len(db.execute('select * from %s where username=%s and password=%s' % (
            TABLE_NAME, Auto_quote(item.username), Auto_quote(item.oldPassword)))) < 1:
        return {
            "code": 1,
            "message": "密码输入错误!",
        }
    elif len(db.execute('select * from %s where username=%s and password=%s and phone=%s' % (
            TABLE_NAME, Auto_quote(item.username), Auto_quote(item.oldPassword), Auto_quote(item.phone)))) < 1:
        return {
            "code": 1,
            "message": "手机号不一致!",
        }
    db.execute(
        'update %s set password=%s where username=%s and password=%s and phone=%s' % (TABLE_NAME,
                                                                                      Auto_quote(item.newPassword),
                                                                                      Auto_quote(item.username),
                                                                                      Auto_quote(item.oldPassword),
                                                                                      Auto_quote(item.phone)
                                                                                      ))
    return {
        "code": 0,
        "message": "修改密码成功!"
    }
