const mongoose = require('../index');
// 创建用户集合规则
const UserSchema = new mongoose.Schema({
  // 字段名/域名称
  name: {
    // 指字域类型
    type: String,
    // 必填字段
    required: true,
  },
  password: String,
  email: String,
});

module.exports = UserSchema;
