const mongoose = require('../index');
// 创建用户集合规则
const NovelSchema = new mongoose.Schema({
  // 字段名/域名称
  title: {
    // 指字域类型
    type: String,
    // 必填字段
    required: true,
  },
  novelConfig: Array,
});

module.exports = NovelSchema;
