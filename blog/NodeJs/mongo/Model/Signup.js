const ModelMan = require('./modelMan');
const SignupSchema = require('../Schema/Signup');
class Signup extends ModelMan {
  // eslint-disable-next-line no-useless-constructor
  constructor(modelName, Schema, dbName) {
    super(modelName, Schema, dbName);
  }
}

module.exports = new Signup('Signup', SignupSchema, 'users');
