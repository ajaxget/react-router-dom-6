const { model } = require('../index');
class ModelMan {
  constructor(modelName, Schema, dbName) {
    this.Model = model(modelName, Schema, dbName);
  }
  findOne(where) {
    return this.Model.findOne(where);
  }

  insertMany(where) {
    return this.Model.insertMany(where);
  }

  updateOne(where, update) {
    return this.Model.updateOne(where, update);
  }
}

module.exports = ModelMan;
