const ModelMan = require('./modelMan');
const LoginSchema = require('../Schema/Login');
class Login extends ModelMan {
  // eslint-disable-next-line no-useless-constructor
  constructor(modelName, Schema, dbName) {
    super(modelName, Schema, dbName);
  }
}

module.exports = new Login('Login', LoginSchema, 'users');
