const ModelMan = require('./modelMan');
const NovelSchema = require('../Schema/Novel');
class Novel extends ModelMan {
  // eslint-disable-next-line no-useless-constructor
  constructor(modelName, Schema, dbName) {
    super(modelName, Schema, dbName);
  }
}

module.exports = new Novel('Novel', NovelSchema, 'novel');
