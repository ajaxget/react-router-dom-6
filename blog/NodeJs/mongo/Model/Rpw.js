const ModelMan = require('./modelMan');
const Rpw_emailSchema = require('../Schema/Rpw');
class Rpw extends ModelMan {
  // eslint-disable-next-line no-useless-constructor
  constructor(modelName, Schema, dbName) {
    super(modelName, Schema, dbName);
  }
}

module.exports = new Rpw('Rpw', Rpw_emailSchema, 'users');
