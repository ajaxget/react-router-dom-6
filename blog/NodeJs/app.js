const express = require('express');
const app = express();
const session = require('cookie-session');
const Routers = require('./Routes/index');

// 中间件引入
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    name: 'sessionId',
    // 给sessionid加密的key,随便填写s
    secret: 'afsfwefwlfjewlfewfef',
    maxAge: 20 * 60 * 1000, // 20分钟
  })
);
// 创建一个静态web服务器
// 现在可以访问public目录下所有的文件
// 如public/index.html文件，则可以通过 : http://xxxx/index.html
app.use(express.static('public'));
app.use(Routers);

app.listen(8080, () => {
  console.log('server is running at http://127.0.0.1:8080');
});
