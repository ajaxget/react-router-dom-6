const NovelConfig = {
  斗罗大陆: [
    {
      URL: [
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/igSRlHML7Ukex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/uq-shwCz0Woex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/HC52sQFik3Uex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/V8lRFUHFN30ex0RJOkJclQ2/',
      ],
      page: 1,
    },
    {
      URL: [
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/0cazGEYNZFEex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/Tm6hvaEsqX4ex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/f50WutR7lEIex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/NcfPhI9PSvwex0RJOkJclQ2/',
      ],
      page: 2,
    },
    {
      URL: [
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/GPRzSusskDAex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/oT_cVH18zrMex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/H7dJE5sB_PAex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/wdKkEb1kmFoex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/Pf1fpQnbFRAex0RJOkJclQ2/',
      ],
      page: 3,
    },
    {
      URL: [
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/xUY3N7N_yqAex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/PKVn8KegE_cex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/iOOnjpA_q2Yex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/797pq6Ccrt8ex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/sST-YrcyPZIex0RJOkJclQ2/',
      ],
      page: 4,
    },
    {
      page: 5,
      URL: [
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/RwhT3E5RCrkex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/m-oX9ioGA38ex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/TQ-nPaQ1SGIex0RJOkJclQ2/',
        'https://read.qidian.com/chapter/YvJ9Xu5KMv01/HuHMLggUxdMex0RJOkJclQ2/',
      ],
    },
  ],
};

module.exports = NovelConfig;
