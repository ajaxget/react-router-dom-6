const tyxsFN = require('./天域小说');

const Novel = (req, res) => {
  /**
   *  @param title 小说名称
   *  @param page  小说章节
   *  @param novelURL 爬取的文件所放的根目录
   */
  const { title, novelURL } = req.body;
  const ManML = `${novelURL}/${title}`;
  tyxsFN(req.body, ManML, res);
};

module.exports = Novel;
