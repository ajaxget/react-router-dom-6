const Model = require('../mongo/Model/Signup');

const Signup = (req, res) => {
  const { username, password, phone, okPassword } = req.body;
  if (password === okPassword) {
    Model.findOne({ name: username }).then((ret) => {
      if (!ret) {
        Model.insertMany({ name: username, password, phone });
        res.send({
          code: 0,
          message: `欢迎${username} 到来!`,
        });
      } else {
        res.send({
          code: 1,
          message: '该用户已被注册!',
        });
      }
    });
  } else {
    res.send({
      code: 1,
      message: '请检查密码是否正确!',
    });
  }
};

module.exports = Signup;
