const Model = require('../mongo/Model/Rpw');

const Rpw = (req, res) => {
  const { username, oldPassword, newPassword, phone } = req.body;
  if (oldPassword === newPassword) {
    res.send({
      code: 1,
      message: '旧密码不可与新密码相同!',
    });
    return;
  }
  Model.updateOne(
    { name: username, password: oldPassword, phone },
    {
      password: newPassword,
    }
  ).then((ret) => {
    if (ret.modifiedCount) {
      res.send({
        code: 0,
        message: '修改成功!',
      });
    } else {
      res.send({
        code: 1,
        message: '没有找到该用户!',
      });
    }
  });
};

module.exports = Rpw;
