const express = require('express');
const Router = express.Router();
const Login = require('./Login');
const Signup = require('./Signup');
const Rpw = require('./Rpw');
const Novel = require('./Crawling novel/index');

// 登录
Router.post('/login', Login);

// 注册
Router.post('/signup', Signup);

// 忘记密码
Router.post('/rpw_email', Rpw);

Router.post('/novel', Novel);

module.exports = Router;
