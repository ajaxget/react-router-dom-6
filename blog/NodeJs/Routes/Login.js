const Model = require('../mongo/Model/Login');
const Login = (req, res) => {
  const { username, password } = req.body;
  Model.findOne({ name: username, password }).then((ret) => {
    if (ret) {
      let message = '';
      if (!req.session[username]) {
        req.session[username] = 1;
        message = `欢迎您首次访问本站点！`;
      } else {
        req.session[username]++;
        message = `欢迎您第 ${req.session[username]} 次访问本站点！`;
      }
      res.send({
        code: 0,
        message,
      });
    } else {
      res.send({
        code: 1,
        message: '密码错误!',
      });
    }
  });
};

module.exports = Login;
