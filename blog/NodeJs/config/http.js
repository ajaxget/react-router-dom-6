const axios = require('axios');

const request = axios.create({
  timeout: 5000,
});

// 响应拦截器
request.interceptors.response.use(
  (ret) => {
    if (ret.data) {
      return ret.data;
    } else {
      return ret;
    }
  },
  (error) => {
    return Promise.reject(error);
  }
);

module.exports = request;
