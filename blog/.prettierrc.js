// 本地配置和当前项目配置
// 优先选择当前项目配置
module.exports = {
  // prettier 设置强制单引号
  singleQuote: true,
  // prettier 设置语句末尾加分号
  semi: true,
  '[javascript]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  '[javascriptreact]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  '[html]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  '[json]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  '[jsonc]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  '[typescript]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
  'gitlens.hovers.currentLine.over': 'line',
  '[typescriptreact]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode',
  },
};
