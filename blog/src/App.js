import React, { useState, Suspense } from 'react';
import classnames from 'classnames';
import { asideConfig } from '@/config/menuConfig';
import _ from 'lodash';
import { NavLink, Outlet, useLocation } from 'react-router-dom';
import CommonCard from '@/common/CommonCard';
import { routerPrefix } from '@/config/config';
import Fallback from '@/page/FallBack';
import { SwitchTransition, CSSTransition } from 'react-transition-group';

const App = ({ menuConfig }) => {
  const [isOpen, setIsOpen] = useState(false);
  const location = useLocation();
  const mnavhClick = () => {
    setIsOpen(!isOpen);
  };
  const asideConfig_pathname = location.pathname.split('/')[2];
  return (
    <>
      <header className="header-navigation slideDown" id="header">
        <nav>
          <div className="logo">
            <a href={routerPrefix}>Ax个人博客</a>
          </div>
          <h2
            id="mnavh"
            className={classnames({
              open: isOpen,
            })}
            onClick={mnavhClick}
          >
            <span className="navicon"></span>
          </h2>
          <ul
            id="starlist"
            className={classnames({
              starlistBlock: isOpen,
            })}
          >
            {_.map(menuConfig, (item, key) => {
              const path = _.isArray(item.path) ? item.path[0].path : item.path;
              return (
                <li key={item.id}>
                  <NavLink
                    className={({ isActive }) => {
                      console.log(isActive, path);
                      return isActive ? 'active' : '';
                    }}
                    to={path || routerPrefix}
                  >
                    {item.name}
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </nav>
      </header>
      <article>
        {!_.isEmpty(asideConfig[asideConfig_pathname]) && (
          <aside className="l_box">
            {_.map(asideConfig[asideConfig_pathname], (item, key) => {
              return (
                <div key={item.id}>
                  <CommonCard title={item.title} {...item.props}>
                    <item.component {...item.componentProps} />
                  </CommonCard>
                </div>
              );
            })}
          </aside>
        )}

        <main
          className={classnames('r_box', {
            r_1_box: _.isEmpty(asideConfig[asideConfig_pathname]),
          })}
        >
          <div className="about">
            {/* <Suspense fallback={<Fallback />}> */}
            {/* </Suspense> */}
            <SwitchTransition>
              <CSSTransition
                key={location.pathname}
                unmountOnExit
                classNames="fade"
                timeout={300}
              >
                <div>
                  <Outlet />
                </div>
              </CSSTransition>
            </SwitchTransition>
          </div>
        </main>
      </article>
      <footer>
        <p>
          Design by&nbsp;
          <a href="https://gitee.com/ajaxget" target="_blank" rel="noreferrer">
            Ax_Gitee
          </a>
        </p>
      </footer>
      <a href="#" className="cd-top">
        Top
      </a>
    </>
  );
};

export default App;
