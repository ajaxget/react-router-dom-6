import React from 'react';
import ReactDOM from 'react-dom';
import App from './router';
import './style/index.less';
import { BrowserRouter as Router } from 'react-router-dom';

ReactDOM.render(
  // 路由前缀
  <Router basename="/">
    <App />
  </Router>,
  document.getElementById('root')
);
