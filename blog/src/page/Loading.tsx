import React, { FC, ReactNode } from 'react';
import Fallback from '@/page/FallBack';

interface IProps {
  children: ReactNode;
  loading_ing?: boolean;
}

const Loading: FC<IProps> = ({ children, loading_ing = false }) => {
  return (
    <>
      {loading_ing && <Fallback mask={true} />}
      {children}
    </>
  );
};

export default Loading;
