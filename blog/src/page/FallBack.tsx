import React, { FC } from 'react';
import classnames from 'classnames';

interface IProps {
  mask?: boolean;
}

const Fallback: FC<IProps> = ({ mask = false }) => {
  return (
    <div
      className={classnames('mask', {
        mask_box: mask,
      })}
    >
      <div className="iconfont icon-LoadingIndicator goog-te-spinner"></div>
    </div>
  );
};

export default Fallback;
