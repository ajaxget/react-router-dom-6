window.addEventListener('scroll', () => {
  const scrollTop = window.scrollY;
  const header = document.querySelector('header');
  const cdTop = document.querySelector('.cd-top');

  if (scrollTop > 100) {
    header.classList.add('slideUp');
    header.classList.remove('slideDown');
    cdTop.classList.add('cd-is-visible');
  } else {
    header.classList.remove('slideUp');
    header.classList.add('slideDown');
    cdTop.classList.remove('cd-is-visible');
  }
});
