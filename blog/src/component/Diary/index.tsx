import React, { FC } from 'react';
import { Route, Routes } from 'react-router-dom';

const Diary: FC = () => {
  return (
    <div>
      Diary
      <Routes>
        <Route path="abc" element={<h1>123</h1>}></Route>
      </Routes>
    </div>
  );
};

export default Diary;
