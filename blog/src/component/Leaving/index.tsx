import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';

const Leaving: FC = () => {
  return (
    <div>
      Leaving
      <Outlet />
    </div>
  );
};

export default Leaving;
