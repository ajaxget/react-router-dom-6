import { NotesConfigProps } from '@/config/interface';
export const NotesConfig: NotesConfigProps[] = [
  {
    id: '#1',
    title: 'React教程',
    img: 'http://8.141.157.121/skin/images/01.jpg',
    message: 'Ax个人收藏笔记(React篇)',
    href: 'Note/details',
  },
];
