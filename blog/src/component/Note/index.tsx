import React, { FC, Key, useEffect, useState } from 'react';
import _ from 'lodash';
import request from '@axios';
import { interfaceConfig } from '@/config/interfaceConfig';

interface dataProps {
  id: number;
  date: string;
  browse: number;
  img: string;
  like: number;
  message: string;
  position: string;
  title: string;
}

const Notes: FC = () => {
  const [data, setData] = useState<dataProps[]>([]);
  useEffect(() => {
    request
      .post(interfaceConfig.get_notes_list, {
        keyword: '',
      })
      .then((ret: any) => {
        if (ret.code === 0) {
          setData(ret.data);
        }
      });
  }, []);
  return (
    <div className="notes">
      <h2 className="hometitle">学无止境</h2>
      <ul>
        {_.map(data, (item: dataProps, key: Key) => {
          return (
            <li key={item.id}>
              <h3 className="blogtitle">
                <a
                  href={'Note/details/' + item.id}
                  target="_blank"
                  rel="noreferrer"
                >
                  {item.title}
                </a>
              </h3>
              <div className="bloginfo">
                <span className="blogpic">
                  <a href={'Note/details/' + item.id} title={item.title}>
                    <img
                      src={
                        item.img || 'http://8.141.157.121/skin/images/01.jpg'
                      }
                      alt={item.title}
                    />
                  </a>
                </span>
                <p>{item.message}</p>
              </div>
              <div className="autor">
                {/* <span className="lm f_l">
                  <a
                    href="/study/1/"
                    title="目录1"
                    target="_blank"
                    className="classname"
                  >
                    目录1
                  </a>
                </span> */}
                <span className="dtime f_l">{item.date}</span>
                <span className="viewnum f_l">浏览（ {item.browse} ）</span>
                <span className="pingl f_l">喜欢（ {item.like} ）</span>
                <span className="f_r">
                  <a href={'Note/details/' + item.id} className="more">
                    阅读原文&gt;&gt;
                  </a>
                </span>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Notes;
