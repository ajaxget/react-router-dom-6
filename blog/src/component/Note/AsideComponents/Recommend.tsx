import React, { FC } from 'react';

const Recommend: FC = () => {
  return (
    <ul>
      <li>
        <b>
          <a href="/study/1/2.html" target="_blank">
            个人博客，属于我的小世界！
          </a>
        </b>
        <p>
          <i>
            <a href="/study/1/2.html" title="个人博客，属于我的小世界！">
              <img
                src="http://www.yangqq.net/d/file/news/life/2018-04-27/762f99f369ae786f970477feeb3b9d77.jpg"
                alt="个人博客，属于我的小世界！"
              />
            </a>
          </i>
          个人博客，用来做什么？我刚开始就把它当做一个我吐槽心情的地方，也就相当于一个网络记事本...
        </p>
      </li>
    </ul>
  );
};

export default Recommend;
