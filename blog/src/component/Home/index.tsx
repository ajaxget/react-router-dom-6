import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { tuijNotes } from './config';

const Home: FC = () => {
  return (
    <div className="home">
      <h2 className="hometitle">特别推荐</h2>
      <ul>
        {_.map(tuijNotes, (item, key) => {
          return (
            <li key={item.id}>
              <div className="tpic">
                <img
                  src="http://www.yangqq.net/d/file/news/life/2014-07-10/43baa4c7c03ed66bae98696de5ebc64e.jpg"
                  title={item.title}
                />
              </div>
              <b>{item.title}</b>
              <span>{item.message}</span>
              <Link to={item.href} className="readmore" target="_blank">
                阅读文章
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Home;
