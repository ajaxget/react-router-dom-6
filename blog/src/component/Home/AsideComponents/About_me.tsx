import React, { FC } from 'react';
import meLog from '@/assets/images/4.jpg';

const About_me: FC = () => {
  console.log(meLog);

  return (
    <div className="about_me">
      <i>
        <img src={meLog} />
      </i>
      <p>
        <b>Ax</b>
        ，一直潜心研究web前端技术，一边工作一边积累经验，以及SEO优化等心得。
      </p>
    </div>
  );
};

export default About_me;
