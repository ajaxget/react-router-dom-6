export const interfaceConfig = {
  get_notes_list: '/notes/get_notes_list', // 获取笔记列表
  get_note: '/notes/get_notes', // 获取某一个笔记
};
