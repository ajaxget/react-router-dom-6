import Home_About_me from '@/component/Home/AsideComponents/About_me';
import Note_Recommend from '@/component/Note/AsideComponents/Recommend';
import Note_Ranking from '@/component/Note/AsideComponents/Ranking';

export default {
  Home_About_me,
  Note_Recommend,
  Note_Ranking,
};
