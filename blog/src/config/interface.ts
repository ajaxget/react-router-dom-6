import { ReactNode } from 'react';
interface menuConfigChildProps {
  id: string;
  name?: string;
  component?: ReactNode;
  path?: string | menuConfigChildProps[];
  componentProps?: {
    index: boolean;
  };
}

export interface menuConfigProps extends menuConfigChildProps {
  children?: menuConfigChildProps[];
}

export interface asideConfigChildProps {
  id: string;
  title: string;
  props: {};
  component: ReactNode | string;
  componentProps: {};
}

export interface asideConfigProps {
  [name: string]: asideConfigChildProps[];
}

export interface NotesConfigProps {
  id: string;
  title: string;
  img: string;
  message: string;
  href: string;
}
