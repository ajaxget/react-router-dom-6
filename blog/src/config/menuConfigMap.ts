import { lazy, memo } from 'react';
import Home from '@/component/Home';
import Album from '@/component/Album';
import Diary from '@/component/Diary';
import About_me from '@/component/About_me';
import Leaving from '@/component/Leaving';
import Note from '@/component/Note';
import CommonNotes_Details_ from '@/common/CommonNote_Details';

// const Home = lazy(() => import('@/component/Home'));
// const Album = lazy(() => import('@/component/Album'));
// const Diary = lazy(() => import('@/component/Diary'));
// const About_me = lazy(() => import('@/component/About_me'));
// const Leaving = lazy(() => import('@/component/Leaving'));
// const Note = lazy(() => import('@/component/Note'));
// const CommonNotes_Details_ = lazy(() => import('@/common/CommonNote_Details'));
import Demo_Leaving from '@/component/Leaving/Demo';

interface menuConfigMapProps {
  [name: string]: any;
}

const menuConfigMap: menuConfigMapProps = {
  Home,
  Album,
  Diary,
  About_me,
  Leaving,
  Note,
  CommonNotes_Details_,
  Demo_Leaving,
};

function memoRenderData() {
  const newData: menuConfigMapProps = {};
  for (let name in menuConfigMap) {
    if (name[name.length - 1] === '_') {
      newData[name] = menuConfigMap[name];
      continue;
    }
    newData[name] = memo(menuConfigMap[name]);
  }
  return newData;
}

export default memoRenderData();
