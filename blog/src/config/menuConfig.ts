import { menuConfigProps, asideConfigProps } from './interface';
import menuConfigMap from '@/config/menuConfigMap';
import asideConfigMap from './asideConfigMap';

export const menuConfig: menuConfigProps[] = [
  {
    id: '#1',
    name: '网站首页',
    component: menuConfigMap.Home,
    // path: 'home',
    componentProps: {
      index: true,
    },
  },
  // {
  //   id: '#2',
  //   name: '我的相册',
  //   path: 'album',
  //   component: menuConfigMap.Album,
  // },
  {
    id: '#3',
    name: '我的日记',
    path: 'diary/*',
    component: menuConfigMap.Diary,
  },
  {
    id: '#4',
    name: '关于我',
    path: 'about_me',
    component: menuConfigMap.About_me,
  },
  {
    id: '#5',
    name: '留言',
    path: 'Leaving',
    component: menuConfigMap.Leaving,
    children: [
      {
        id: '#5-1',
        path: 'abc',
        component: menuConfigMap.Demo_Leaving,
      },
    ],
  },
  {
    id: '#6',
    name: '笔记',
    path: [
      {
        id: '#6-1',
        path: 'Note',
        component: menuConfigMap.Note,
      },
      {
        id: '#6-2',
        path: 'Note/details/:id',
        component: menuConfigMap.CommonNotes_Details_,
      },
    ],
  },
];

export const asideConfig: asideConfigProps = {
  '': [
    {
      id: '#1',
      title: '关于我',
      props: {},
      component: asideConfigMap.Home_About_me,
      componentProps: {},
    },
  ],
  Note: [
    {
      id: '#1',
      title: '本栏推荐',
      props: {
        title_class: 'ab_title',
      },
      component: asideConfigMap.Note_Recommend,
      componentProps: {},
    },
    {
      id: '#2',
      title: '本栏排行',
      props: {
        title_class: 'ab_title',
      },
      component: asideConfigMap.Note_Ranking,
      componentProps: {},
    },
  ],
};
