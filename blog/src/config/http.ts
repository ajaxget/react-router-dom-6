import axios from 'axios';

const baseURL = '/Ax-blog';
const request = axios.create({
  baseURL,
});

request.interceptors.response.use(
  (ret) => {
    if (ret.data) {
      return ret.data;
    }
    return ret;
  },
  (error) => {
    return Promise.reject(error);
  }
);

const baseURL_md = process.env.NODE_ENV === 'development' ? '/md' : '/skin/md';

const md_request = axios.create({
  baseURL: baseURL_md,
});

md_request.interceptors.response.use(
  (ret) => {
    if (ret.data) {
      return ret.data;
    }
    return ret;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export { md_request };

export default request;
