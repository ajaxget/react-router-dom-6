import React, { FC, ReactNode } from 'react';

interface IProps {
  children?: ReactNode;
  title: string;
  title_class?: string;
}

const CommonCard: FC<IProps> = ({ children, title, title_class }) => {
  return (
    <div className="common-card">
      <h2 className={title_class}>{title}</h2>
      <div className="common-card-body">{children}</div>
    </div>
  );
};

export default CommonCard;
