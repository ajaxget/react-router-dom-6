import React, { FC, useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';
import MarkdownIt from 'markdown-it';
import MdEditor from 'react-markdown-editor-lite';
// import style manually
import 'react-markdown-editor-lite/lib/index.css';
import RehypeRaw from 'rehype-raw';
import request from '@axios';
import { interfaceConfig } from '@/config/interfaceConfig';
import { useParams } from 'react-router-dom';
import Loading from '@/page/Loading';

interface IProps {}

const CommonNote_details: FC<IProps> = () => {
  const [data, setData] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true);
  const { id } = useParams();
  useEffect(() => {
    request
      .post(interfaceConfig.get_note, { id })
      .then((ret: any) => {
        if (ret.code === 0) {
          setData(ret.data[0].data);
        }
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);
  const components = {
    code({ node, inline, className, children, ...props }: any) {
      const match = /language-(\w+)/.exec(className || '');
      return !inline && match ? (
        <SyntaxHighlighter
          style={dark}
          language={match[1]}
          PreTag="div"
          showLineNumbers={true}
          // eslint-disable-next-line react/no-children-prop
          children={String(children).replace(/\n$/, '')}
          {...props}
        />
      ) : (
        <code className="dmk" {...props}>
          {String(children).replace(/\n$/, '')}
        </code>
      );
    },
  };
  const mdParser = new MarkdownIt(/* Markdown-it options */);
  // Finish!
  function handleEditorChange({ html, text }: { html: string; text: string }) {
    console.log('handleEditorChange', html, text);
  }

  return (
    <Loading loading_ing={loading}>
      <div id="note_details">
        <ReactMarkdown
          components={components}
          remarkPlugins={[gfm]}
          rehypePlugins={[RehypeRaw]}
          // eslint-disable-next-line react/no-children-prop
          children={data}
        />
        {/* <MdEditor
  style={{ height: '500px' }}
  renderHTML={(text: any) => mdParser.render(text)}
  onChange={handleEditorChange}
/> */}
      </div>
    </Loading>
  );
};

export default CommonNote_details;
