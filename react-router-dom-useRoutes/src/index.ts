import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './Route';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConfigProvider, App as App_ } from 'antd';
import 'antd/dist/reset.css';
import './index.less';

const dom = document.getElementById('root');
if (dom) {
  const root = createRoot(dom);
  root.render(
    React.createElement(
      ConfigProvider,
      {},
      React.createElement(
        App_,
        {
          style: {
            height: '100%',
          },
        },
        React.createElement(Router, {}, React.createElement(App))
      )
    )
  );
}
