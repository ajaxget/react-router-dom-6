import { FC } from 'react';

const NotFound: FC = () => {
  return <>404</>;
};

export default NotFound;
