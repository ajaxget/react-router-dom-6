import { FC } from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input, App } from 'antd';
import { setCookie } from '@/utils/cookie';
import { useNavigate } from 'react-router-dom';
import './index.less';

const Login: FC = () => {
  const navigate = useNavigate();
  const { message } = App.useApp();

  const onFinish = (values: any) => {
    setCookie('username', values.name, 60 * 60 * 2);
    navigate('/');
    message.success('登录成功');
  };

  return (
    <div className="normal">
      <h1>登 录</h1>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          name: 'zhangsan',
          password: '123123',
        }}
        onFinish={onFinish}
      >
        <Form.Item name="name" rules={[{ required: true, message: 'Please input your Username!' }]}>
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }]}>
          <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
