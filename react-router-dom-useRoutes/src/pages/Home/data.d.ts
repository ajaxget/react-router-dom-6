export interface dataProps {
  name: string;
  course: string;
  id: string | number;
}
