import { FC } from 'react';
import { Table } from 'antd';
import { tableConfig } from './config';
import { dataProps } from './data';

const Home: FC = () => {
  return (
    <>
      <Table<dataProps>
        columns={tableConfig}
        dataSource={[
          {
            name: 'zhangsan',
            course: '语文',
            id: '1',
          },
        ]}
        rowKey="id"
      />
    </>
  );
};

export default Home;
