import type { ColumnsType } from 'antd/es/table';
import { dataProps } from './data';

export const tableConfig: ColumnsType<dataProps> = [
  {
    title: '姓名',
    dataIndex: 'name',
  },
  {
    title: '课程',
    dataIndex: 'course',
  },
];
