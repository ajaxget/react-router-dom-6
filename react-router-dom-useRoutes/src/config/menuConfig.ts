import { lazy } from 'react';
import { menuConfigMap } from './menuConfigMap';
import { Navigate } from 'react-router-dom';

export const menuConfig = [
  {
    id: '#1',
    title: '课程',
    path: '/kc',
    component: menuConfigMap.Home,
    index: true,
    lazy: true,
  },
];

export const routes = [
  {
    path: '/',
    component: Navigate,
    lazy: false,
    componentProps: {
      to: '/kc',
    },
  },
  {
    path: '/',
    component: lazy(() => import('@/App')),
    children: menuConfig,
    lazy: true,
  },
  {
    path: '/sign_in',
    component: lazy(() => import('@/pages/Login')),
    lazy: true,
  },
  {
    path: '*',
    component: lazy(() => import('@/pages/404')),
    lazy: true,
  },
];
