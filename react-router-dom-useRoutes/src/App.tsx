import { FC } from 'react';
import { Avatar, Button, Layout, Menu, Popover, theme } from 'antd';
import { menuConfig } from './config/menuConfig';
import { Outlet } from 'react-router-dom';
import avatar1 from '@/assets/img/avatar1.jpg';

const { Header, Content, Footer, Sider } = Layout;

const App: FC = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Layout className="layout">
      <Sider breakpoint="lg" collapsedWidth="0">
        <div className="logo-vertical" />
        <Menu
          theme="dark"
          mode="inline"
          items={menuConfig.map((item) => ({
            key: item.id,
            label: item.title,
          }))}
          defaultSelectedKeys={['#1']}
        />
      </Sider>
      <Layout>
        <Header className="header" style={{ background: colorBgContainer }}>
          <div className="left"></div>
          <div className="right">
            <Popover
              content={
                <div>
                  <Button
                    type="primary"
                    onClick={() => {
                      document.cookie = 'username=; path=/;';
                      window.location.reload();
                    }}
                  >
                    登出
                  </Button>
                </div>
              }
            >
              <Avatar src={avatar1} className="avatar" />
            </Popover>
          </div>
        </Header>
        <Content style={{ margin: '24px 16px 0' }}>
          <div style={{ padding: 24, height: '100%', background: colorBgContainer }}>
            <Outlet />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>XXX XXX XXX XXX XXX</Footer>
      </Layout>
    </Layout>
  );
};

export default App;
