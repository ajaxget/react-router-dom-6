// 获取cookie 中键值对
export function getCookie() {
  let obj: { [name: string]: any } = {};
  const arr = document.cookie.split('; ');
  arr.forEach((v: string) => {
    const arr1 = v.split('=');
    const name = arr1[0];
    obj[name] = arr1[1];
  });
  return obj;
}

//设定 cookie
// key --- 键名
// value --- 键值
// endTime  --- 失效时间
export function setCookie(key: string, value: string | number, endTime: number) {
  const time = new Date();
  const t = time.getTime() - 8 * 60 * 60 * 1000 + endTime * 1000;
  time.setTime(t);
  document.cookie = `${key}=${value};expires=${endTime ? time : ' '};path=/`;
}
