import axios from 'axios';

const request = axios.create({
  baseURL: '/monitor',
});

request.interceptors.response.use((config) => {
  if (config.data) config.data;
  return config;
});

export default request;
