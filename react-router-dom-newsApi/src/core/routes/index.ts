import { createBrowserRouter } from 'react-router-dom';

export const routes = createBrowserRouter([
  {
    path: '/',
    lazy: () => import('../../App'),
    children: [
      {
        path: '/home',
        lazy: () => import('../../view/Home'),
        children: [
          {
            path: ':id',
            lazy: () => import('../../view/Home/c'),
          },
        ],
      },
      {
        path: '/about',
        lazy: () => import('../../view/About'),
      },
    ],
  },
]);
