import { FC } from 'react';
import { useAsyncValue } from 'react-router-dom';

const AwaitChild: FC = () => {
  const value = useAsyncValue();
  console.log(value);

  return <div>revi</div>;
};
export default AwaitChild;
