import { FC } from 'react';

export const Component: FC = () => {
  return <>About</>;
};

export const loader = async () => {
  console.log('about loader');

  return null;
};

export const action = async () => {
  console.log('about action');

  return null;
};
