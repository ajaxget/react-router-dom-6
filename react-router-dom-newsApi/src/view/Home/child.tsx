import { FC } from 'react';
import { useFetcher } from 'react-router-dom';

const Index: FC = () => {
  const f = useFetcher();
  return (
    <f.Form method="post">
      <input name="child_name" />
    </f.Form>
  );
};

export default Index;
