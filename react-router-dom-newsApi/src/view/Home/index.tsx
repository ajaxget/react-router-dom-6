import { FC } from 'react';
import {
  Form,
  useFetcher,
  useLocation,
  useActionData,
  useOutlet,
  useFetchers,
  Link,
} from 'react-router-dom';
import Child from './child';

export const Component: FC = () => {
  const fetch = useFetcher();
  const location = useLocation();
  const action = useActionData();
  const outlet = useOutlet();
  const fetchers = useFetchers();
  console.log(fetchers, 'fetchers');

  console.log(location, 'location');

  console.log(action, 'action');

  console.log(fetch.data, 'fetch.data');
  console.log(
    fetch.formData?.get('title'),
    fetch.formData?.get('name'),
    'fetch.formData?.get'
  );
  console.log(fetch.state, 'fetch.state');

  return (
    <>
      Home
      <Form method="post" state={{ state_1: '1' }}>
        <input name="title" />
      </Form>
      <fetch.Form method="post">
        <input type="text" name="title" />
      </fetch.Form>
      <button
        onClick={() => {
          fetch.load(location.pathname);
        }}
      >
        重新发送 loader
      </button>
      <button
        onClick={() => {
          fetch.submit(
            { n: 11 },
            {
              method: 'post',
            }
          );
        }}
      >
        重新发送 action
      </button>
      <hr />
      {outlet}
      <hr />
      <Child />
      <Child />
      <Child />
      <hr />
      <h1>
        测试 <code>unstable_useViewTransitionState</code>
      </h1>
      <Link to="/about" unstable_viewTransition>
        goto about
      </Link>
    </>
  );
};

export const loader = async ({ request, params, context }) => {
  console.log('home loader');

  return { home_loader: '111' };
};

export const action = async ({ request, params, context }) => {
  const data = await request.formData();
  console.log('home action');

  await new Promise((reslove) => {
    setTimeout(() => {
      reslove(1);
    }, 2000);
  });
  return { name: data.get('title'), child_name: data.get('child_name') };
};
